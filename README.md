# Slackware-NWG

My repo for running Hyprland, Sway & the NWG toolkit on Slackware-Current.
The iso to start off with can be found at https://rekt.lngn.net/nwg/ and information on how to install can be found in the 
nwg wiki at https://github.com/nwg-piotr/nwg-shell/wiki.
The folder slackpkg in this repo contains some Slackware specific settings.
Thanks to Jay and Piotr for creating this excellent spin!

Feel free to use these builds and dots. I do not, however, offer or imply any form of support or ongoing maintenance. And of course, you use them entirely at your own risk. Have fun!
