# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='exa -l -a --group-directories-first --icons -@'
alias hy='dbus-run-session Hyprland'
alias sw='dbus-run-session sway'

PS1='[\u@\h \W]\$ '
